//
//  AppDelegate.m
//  xerox
//
//  Created by Jeisson González on 10/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "AppDelegate.h"
#import "Services.h"
#import <OneSignal/OneSignal.h>
#import "GNGeoHash.h"


#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <GBDeviceInfo/GBDeviceInfo.h>
@import INTULocationManager;

@import Firebase;
@import FirebaseFirestore;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
    
    
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:@"6bcbb939-98f7-4d4b-b319-e5fc16443d6c"
            handleNotificationAction:^(OSNotificationOpenedResult *result) {
                
                // This block gets called when the user opens or taps an action on a notification
                OSNotificationPayload* payload = result.notification.payload;
                NSLog(@"payload: %@",payload);
                
            }
                            settings:@{kOSSettingsKeyAutoPrompt: @true}];
    
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    [FIRApp configure];
    FIRFirestore *defaultFirestore = [FIRFirestore firestore];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSUUID *UUID = [NSUUID UUID];
    NSString *stringUUID = [UUID UUIDString];
    [defaults setObject:stringUUID forKey:@"stringUUID"];
    [defaults synchronize];
    
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]) {
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr subscribeToSignificantLocationChangesWithBlock:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
            // This block will be executed with the details of the significant location change that triggered the background app launch,
            // and will continue to execute for any future significant location change events as well (unless canceled).
            NSLog(@"currentLocation: %@",currentLocation);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sdkgps" object:nil];
        }];
    }
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"sdkgps" object:nil queue:nil usingBlock:^(NSNotification *notification){
        [self enviarInfoGP];
    }];
    
    return YES;
}

-(void)enviarInfoGP{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *localizacion = [defaults objectForKey:@"localizacion"];
    NSString *stringUUID = [defaults objectForKey:@"stringUUID"];
    
    
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    
    NSString *appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *urlServicio = [NSString stringWithFormat:@"https://sdk-gps-981fe.firebaseio.com/8af7cb10-2c03-11e9-9d75-6741c28775dd/%@.json",stringUUID];
    
    NSDictionary *dataCelular=[self getData];
    
    
    GNGeoHash *gh = [GNGeoHash withCharacterPrecision:[localizacion[@"latitude"] floatValue] andLongitude:[localizacion[@"longitude"] floatValue] andNumberOfCharacters:12];
    
    //print the result
    NSLog(@"GeoHash is %@",[gh toBase32]);
    
    NSDictionary *dataSend = @{
                               @"geohash": [NSString stringWithFormat:@"%@",[gh toBase32]],
                               @"latitude": localizacion[@"latitude"],
                               @"longitude": localizacion[@"longitude"],
                               @"speed": localizacion[@"speed"],
                               @"altitude": localizacion[@"altitude"],
                               @"batteryPercentage": dataCelular[@"bateriaDisponible"],
                               @"appversion": appVersionString,
                               @"device": @"iPhone",
                               @"deviceModel": dataCelular[@"modelo"],
                               @"osVersion": dataCelular[@"OS"],
                               @"networkOperator":  [NSString stringWithFormat:@"%@",[carrier carrierName]],
                               @"deviceLanguage":dataCelular[@"langue"]
                               };
    
    [Services post:urlServicio parameters:dataSend completion:^(BOOL finished, NSDictionary *res) {
                                                NSLog(@"res %@",res);
                                            }];
}

-(NSDictionary *)getData{
    
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    
    
    //BATERIA
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    
    double batLeft = (float)[myDevice batteryLevel] * 100;
    
    
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
        
    
    GBDeviceInfo *deviceInfo = [GBDeviceInfo deviceInfo];
    data[@"bateriaDisponible"]=[NSString stringWithFormat:@"%.1f",batLeft];
    data[@"modelo"]=[NSString stringWithFormat:@"%@",deviceInfo.modelString];
    data[@"OS"]=[NSString stringWithFormat:@"%i",deviceInfo.osVersion.major];
    data[@"langue"] = [NSString stringWithFormat:@"%@",language];
    
    return data;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
