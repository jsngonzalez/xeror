//
//  ContactoViewController.h
//  xerox
//
//  Created by Jeisson González on 11/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "CustomTextView.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface ContactoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet CustomTextField *txtNombre;
@property (strong, nonatomic) IBOutlet CustomTextField *txtApellido;
@property (strong, nonatomic) IBOutlet CustomTextField *txtEmpresa;
@property (strong, nonatomic) IBOutlet CustomTextField *txtTelefono;
@property (strong, nonatomic) IBOutlet CustomTextView *txtMensaje;
@property (strong, nonatomic) IBOutlet CustomTextField *txtCorreo;
@property (strong, nonatomic) IBOutlet UIButton *btnSuscribir;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollview;
@end
