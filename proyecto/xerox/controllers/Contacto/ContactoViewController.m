//
//  ContactoViewController.m
//  xerox
//
//  Created by Jeisson González on 11/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "ContactoViewController.h"
#import "FCAlertView.h"
#import "Services.h"
#import "SVProgressHUD.h"

@import Firebase;
@import FirebaseFirestore;


@interface ContactoViewController ()
@end

@implementation ContactoViewController{
    NSString *resValidar;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [FIRAnalytics logEventWithName:@"contacto" parameters:nil];
    
    CGRect frame = _bg.frame;
    frame.size.width = SCREEN_WIDTH + 30;
    _bg.frame = frame;
    
    [UIView animateWithDuration:0.8f
                     animations:^{
                         CGRect frame = _bg.frame;
                         frame.size.width = SCREEN_WIDTH+2;
                         _bg.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    [_scrollview contentSizeToFit];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)atras:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)enviarMensaje:(id)sender {
    
    
    [self.view endEditing:YES];
    if ([self validar]) {
        
        [SVProgressHUD show];
        
        NSDictionary *data=@{
                             @"nombre":_txtNombre.text,
                             @"apellido":_txtApellido.text,
                             @"correo":_txtCorreo.text,
                             @"empresa":_txtEmpresa.text,
                             @"telefono":_txtTelefono.text,
                             @"mensaje":_txtMensaje.text,
                             @"suscripcion":[NSNumber numberWithInt:_btnSuscribir.tag]
                             };
        
        FIRFirestore *db = [FIRFirestore firestore];
        [[db collectionWithPath:@"contacto"] addDocumentWithData:data completion:^(NSError * _Nullable error) {
            
            [SVProgressHUD dismiss];
            if (error != nil) {
                FCAlertView *alert = [[FCAlertView alloc] init];
                
                [alert makeAlertTypeWarning];
                
                [alert showAlertInView:self
                             withTitle:nil
                          withSubtitle:@"Error enviando la información"
                       withCustomImage:nil
                   withDoneButtonTitle:@"OK"
                            andButtons:nil];
            } else {
                
                FCAlertView *alert = [[FCAlertView alloc] init];
                
                [alert makeAlertTypeSuccess];
                
                [alert showAlertInView:self
                             withTitle:nil
                          withSubtitle:@"Información enviada exitosamente."
                       withCustomImage:nil
                   withDoneButtonTitle:@"OK"
                            andButtons:nil];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
        
    }else{
        
        FCAlertView *alert = [[FCAlertView alloc] init];
        
        [alert makeAlertTypeWarning];
        
        [alert showAlertInView:self
                     withTitle:nil
                  withSubtitle:resValidar
               withCustomImage:nil
           withDoneButtonTitle:@"OK"
                    andButtons:nil];
    }
   
    
}

-(BOOL) validar{

    BOOL ok=YES;
    if ([_txtNombre.text isEqualToString:@""]) {
        resValidar=@"Debe ingresar su nombre";
        ok=NO;
    }else if ([_txtApellido.text isEqualToString:@""]) {
        resValidar=@"Debe ingresar su apellido";
        ok=NO;
    }else if ([_txtCorreo.text isEqualToString:@""]) {
        resValidar=@"Debe ingresar su correo";
        ok=NO;
    }else if ([_txtTelefono.text isEqualToString:@""]) {
        resValidar=@"Debe ingresar su número";
        ok=NO;
    }else if ([_txtMensaje.text isEqualToString:@""]) {
        resValidar=@"Debe ingresar un mensaje";
        ok=NO;
    }
    
    return ok;
}

- (IBAction)suscribirme:(id)sender {
    if (_btnSuscribir.tag==0) {
        _btnSuscribir.tag=1;
        [_btnSuscribir setImage:[UIImage imageNamed:@"check-box-selected"] forState:UIControlStateNormal];
    }else{
        _btnSuscribir.tag=0;
        [_btnSuscribir setImage:[UIImage imageNamed:@"check-box-empty"] forState:UIControlStateNormal];
    }
}
@end
