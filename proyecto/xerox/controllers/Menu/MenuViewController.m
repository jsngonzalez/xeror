//
//  MenuViewController.m
//  xerox
//
//  Created by Jeisson González on 10/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "MenuViewController.h"
#import "CellGenerica.h"

#import "TGRImageViewController.h"
#import "TGRImageZoomAnimationController.h"
#import "TOWebViewController.h"

@import Firebase;
@import INTULocationManager;

#define cell_header @"CellHeader"
#define cell_menu @"CellMenu"
#define cell_footer @"CellFooter"


#define seg_nosotros @"seg_nosotros"
#define seg_equipos @"seg_equipos"
#define seg_contacto @"seg_contacto"
#define seg_encuesta @"seg_encuesta"


@interface MenuViewController ()

@end

@implementation MenuViewController{
    NSMutableArray *container;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    [FIRAnalytics logEventWithName:@"home" parameters:nil];
    
}

-(void)initView{
    _table.separatorColor = [UIColor clearColor];
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    //_table.backgroundColor=[UIColor clearColor];
    
    if (IS_IPHONE_6 || IS_IPHONE_6P) {
        _table.scrollEnabled=NO;
    }
    
    container=[[NSMutableArray alloc]initWithArray:@[
                                                     @{
                                                         @"cell":cell_header,
                                                         },
                                                     @{
                                                         @"cell":cell_menu,
                                                         @"seg":seg_nosotros,
                                                         @"nombre":@"Bienvenida",
                                                         @"imagen":[UIImage imageNamed:@"bienvenida"]
                                                         },
                                                     @{
                                                         @"cell":cell_menu,
                                                         @"seg":seg_equipos,
                                                         @"nombre":@"Equipos Xerox",
                                                         @"imagen":[UIImage imageNamed:@"equipos"]
                                                         },
                                                     @{
                                                         @"cell":cell_menu,
                                                         @"seg":seg_contacto,
                                                         @"nombre":@"Contáctanos",
                                                         @"imagen":[UIImage imageNamed:@"contacto"]
                                                         },
                                                     @{
                                                         @"cell":cell_menu,
                                                         @"seg":seg_encuesta,
                                                         @"nombre":@"Encuesta",
                                                         @"imagen":[UIImage imageNamed:@"encuesta"]
                                                         },
                                                     @{
                                                         @"cell":cell_footer,
                                                         },
                                                     ]];
    
    [_table reloadData];
    
    
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr subscribeToSignificantLocationChangesWithBlock:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        if (status == INTULocationStatusSuccess) {
            // A new updated location is available in currentLocation, and achievedAccuracy indicates how accurate this particular location is.
            NSLog(@"currentLocation: %@",currentLocation);
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@{
                                  @"latitude":[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude],
                                  @"longitude":[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude],
                                  @"speed":[NSString stringWithFormat:@"%f",currentLocation.speed],
                                  @"altitude":[NSString stringWithFormat:@"%f",currentLocation.altitude]
                                  } forKey:@"localizacion"];
            [defaults synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sdkgps" object:nil];
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableViewDalegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [container count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item=[container objectAtIndex:indexPath.row];
    
    CellGenerica *cell =[tableView dequeueReusableCellWithIdentifier:item[@"cell"]];
    
    cell.btn1.layer.borderColor=[[UIColor whiteColor] CGColor];
    cell.btn1.layer.borderWidth=1;
    
    [cell.btn1 addTarget:self
                 action:@selector(abrirMapa)
       forControlEvents:UIControlEventTouchUpInside];
    
    if ([item[@"cell"] isEqualToString:cell_menu]) {
        cell.label1.text=item[@"nombre"];
        cell.img1.image=item[@"imagen"];
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *item=[container objectAtIndex:indexPath.row];
    
    if ([item[@"cell"] isEqualToString:cell_header]) {
        return 112;
    }else if([item[@"cell"] isEqualToString:cell_menu]){
        return 60;
    }else{
        return 78;
    }
    
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *item=[container objectAtIndex:indexPath.row];
    NSLog(@"item:%@",item);
    
    if ([item[@"cell"] isEqualToString:cell_menu]) {
        [self performSegueWithIdentifier:item[@"seg"] sender:self];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)abrirMapa{
    NSLog(@"Se abre el mapa");
    
    [FIRAnalytics logEventWithName:@"link_home" parameters:@{
                                                                       @"link":@"https://www.xerox.com/es-mx/impresion-digital/expografica"
                                                                       }];
    
    TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:[NSURL URLWithString:@"https://www.xerox.com/es-mx/impresion-digital/expografica"]];
    
    webViewController.doneButtonTitle=@"Cerrar";
    webViewController.navigationButtonsHidden=YES;
    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:webViewController];
    
    
    [self presentViewController:nav animated:YES completion:nil];
}

@end
