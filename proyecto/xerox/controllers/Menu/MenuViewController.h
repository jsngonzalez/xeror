//
//  MenuViewController.h
//  xerox
//
//  Created by Jeisson González on 10/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController<UITabBarDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *table;

@end
