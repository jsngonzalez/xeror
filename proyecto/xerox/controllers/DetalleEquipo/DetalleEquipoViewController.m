//
//  DetalleEquipoViewController.m
//  xerox
//
//  Created by Jeisson González on 11/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "DetalleEquipoViewController.h"
#import "TOWebViewController.h"

@import Firebase;



@interface DetalleEquipoViewController ()

@end

@implementation DetalleEquipoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lblTitulo.text=_item[@"nombre"];
    _lblDetalle.text=_item[@"detalle"];
    
    _btnMasInformacion.layer.borderColor=[[UIColor whiteColor] CGColor];
    _btnMasInformacion.layer.borderWidth=1;

    _btnVideo.layer.borderColor=[[UIColor whiteColor] CGColor];
    _btnVideo.layer.borderWidth=1;
    
    _imgProducto.image=_item[@"imagen"];
    

    
    [FIRAnalytics logEventWithName:@"detalle" parameters:@{
                                                           @"nombre":_item[@"nombre"]
                                                           }];
    
    CGSize size = [_lblDetalle sizeThatFits:CGSizeMake(_lblDetalle.frame.size.width, CGFLOAT_MAX)];
    CGRect frame = _lblDetalle.frame;
    frame.size.height = size.height+10;
    _lblDetalle.frame = frame;
    
    
    frame = _btnVideo.frame;
    frame.origin.y=_lblDetalle.frame.origin.y+_lblDetalle.frame.size.height+10;
    _btnVideo.frame=frame;
    
    if ([_item[@"video"] isEqualToString:@""]) {
        _btnVideo.layer.opacity=0;
    }
    
    if ([_item[@"mas_informacion"] isEqualToString:@""]) {
        _btnMasInformacion.layer.opacity=0;
    }else{
        if ([_item[@"video"] isEqualToString:@""]) {
            frame = _btnVideo.frame;
            _btnMasInformacion.frame=frame;
        }else{
            frame = _btnMasInformacion.frame;
            frame.origin.y=_btnVideo.frame.origin.y+_btnVideo.frame.size.height+10;
            _btnMasInformacion.frame=frame;
        }
    }
    
    [_scrollview contentSizeToFit];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)verVideo:(id)sender {
    
    
    [FIRAnalytics logEventWithName:@"link_verVideo" parameters:@{
                                                           @"video":_item[@"video"]
                                                           }];
    
    TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:[NSURL URLWithString:_item[@"video"]]];
    
    webViewController.doneButtonTitle=@"Cerrar";
    webViewController.navigationButtonsHidden=YES;
    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:webViewController];
    
    
    [self presentViewController:nav animated:YES completion:nil];
}
- (IBAction)masInformacion:(id)sender {
    
    
    [FIRAnalytics logEventWithName:@"link_masInformacion" parameters:@{
                                                            @"link":_item[@"mas_informacion"]
                                                            }];
    
    TOWebViewController *webViewController = [[TOWebViewController alloc] initWithURL:[NSURL URLWithString:_item[@"mas_informacion"]]];
    
    webViewController.doneButtonTitle=@"Cerrar";
    webViewController.navigationButtonsHidden=YES;
    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:webViewController];
    
    
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)atras:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
