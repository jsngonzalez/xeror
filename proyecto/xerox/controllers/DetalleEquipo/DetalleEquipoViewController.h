//
//  DetalleEquipoViewController.h
//  xerox
//
//  Created by Jeisson González on 11/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface DetalleEquipoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UILabel *lblDetalle;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UIButton *btnMasInformacion;
@property (strong, nonatomic) IBOutlet UIButton *btnVideo;
@property (strong, nonatomic) IBOutlet UIImageView *imgProducto;

@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollview;

@property (strong,nonatomic) NSDictionary *item;
@end
