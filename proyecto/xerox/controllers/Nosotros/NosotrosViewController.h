//
//  NosotrosViewController.h
//  xerox
//
//  Created by Jeisson González on 10/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NosotrosViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *lblDetalle;
@property (strong, nonatomic) IBOutlet UIImageView *bg;
@end
