//
//  NosotrosViewController.m
//  xerox
//
//  Created by Jeisson González on 10/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "NosotrosViewController.h"

@import Firebase;


@interface NosotrosViewController ()

@end

@implementation NosotrosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [FIRAnalytics logEventWithName:@"Nosotros" parameters:nil];
}

-(void)initView{
    
    
    CGRect frame = _bg.frame;
    frame.size.width = SCREEN_WIDTH + 30;
    _bg.frame = frame;
    
    [UIView animateWithDuration:0.8f
                     animations:^{
                         CGRect frame = _bg.frame;
                         frame.size.width = SCREEN_WIDTH+2;
                         _bg.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    
    CGSize size = [_lblDetalle sizeThatFits:CGSizeMake(_lblDetalle.frame.size.width, CGFLOAT_MAX)];
    frame = _lblDetalle.frame;
    frame.size.height = size.height+10;
    _lblDetalle.frame = frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)atras:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
