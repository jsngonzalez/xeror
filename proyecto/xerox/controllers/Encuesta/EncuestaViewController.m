//
//  EncuestaViewController.m
//  xerox
//
//  Created by Jeisson González on 11/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "EncuestaViewController.h"
#import "CellGenerica.h"
#import "FCAlertView.h"
#import "Services.h"
#import "SVProgressHUD.h"

@import Firebase;
@import FirebaseFirestore;


@interface EncuestaViewController ()

@end

@implementation EncuestaViewController{
    NSMutableArray *container;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [FIRAnalytics logEventWithName:@"encuesta" parameters:nil];
    
    CGRect frame = _bg.frame;
    frame.size.width = SCREEN_WIDTH + 30;
    _bg.frame = frame;
    
    [UIView animateWithDuration:0.8f
                     animations:^{
                         CGRect frame = _bg.frame;
                         frame.size.width = SCREEN_WIDTH+2;
                         _bg.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    _table.separatorColor = [UIColor clearColor];
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table.backgroundColor=[UIColor clearColor];
    
    container=[[NSMutableArray alloc]initWithArray:@[
                                                     @{
                                                         @"cell":@"CellHeader"
                                                         },
                                                     @{
                                                         @"cell":@"CellPregunta",
                                                         @"texto":@"¿Qué tipo de tecnologías buscas?"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Xerox Brenva - Inyección de tinta en hoja cortada",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Xerox Trivor- Inyección de tinta en alimentación continua",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Xerox Iridesse - Prensa Digital CMYK+",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Monocromático",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Familia Versant",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Impresión em 3D",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellPregunta",
                                                         @"texto":@"¿Qué segmento es en el que te especializas?"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Editorial",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Comercial",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Transaccional",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellRespuesta",
                                                         @"texto":@"Empaque",
                                                         @"estado":@"0"
                                                         },
                                                     @{
                                                         @"cell":@"CellBoton"
                                                         }
                                                     ]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableViewDalegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [container count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item=[container objectAtIndex:indexPath.row];
    
    
    CellGenerica *cell =[tableView dequeueReusableCellWithIdentifier:item[@"cell"]];
    cell.backgroundColor=[UIColor clearColor];
    
    if ([item[@"cell"] isEqualToString:@"CellPregunta"] ) {
        cell.label1.text=item[@"texto"];
    }else if ([item[@"cell"] isEqualToString:@"CellRespuesta"] ) {
        cell.label1.text=item[@"texto"];
        
        if ([item[@"estado"] isEqualToString:@"0"]) {
            cell.img1.image=[UIImage imageNamed:@"check-box-empty"];
        }else{
            cell.img1.image=[UIImage imageNamed:@"check-box-selected"];
        }
    }
    
    
    cell.btn1.layer.cornerRadius=5;
    
    [cell.btn1 addTarget:self
                  action:@selector(responder)
        forControlEvents:UIControlEventTouchUpInside];
    
    cell.backgroundColor=[UIColor clearColor];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *item=[container objectAtIndex:indexPath.row];
    
    if ([item[@"cell"] isEqualToString:@"CellHeader"] ) {
        return 75;
    }else if ([item[@"cell"] isEqualToString:@"CellPregunta"] ) {
        return 55;
    }else if ([item[@"cell"] isEqualToString:@"CellBoton"] ) {
        return 57;
    }else {
        
        if ([item[@"texto"] length] > 40) {
            return 43;
        }else{
            return 35;
        }
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *seleccionado=[container objectAtIndex:indexPath.row];
    
    if ([seleccionado[@"cell"] isEqualToString:@"CellRespuesta"]) {
        NSLog(@"item:%@",seleccionado);
        
        NSMutableArray *temp=[[NSMutableArray alloc]init];
        for (NSDictionary *item in container) {
            
            if ([item[@"texto"] isEqualToString:seleccionado[@"texto"]]) {
                
                NSString *estado=@"0";
                if ([item[@"estado"] isEqualToString:@"0"]) {
                    estado=@"1";
                }
                
                [temp addObject:@{
                                  @"cell":seleccionado[@"cell"],
                                  @"texto":seleccionado[@"texto"],
                                  @"estado":estado
                                  }];
                
            }else{
                [temp addObject:item];
            }
        }
        
        container=temp;
        [tableView reloadData];
    }
    
}

-(void)responder{
NSLog(@"Responder");
    
    BOOL enviar=NO;
    
    for (NSDictionary *item in container) {
        if ([item[@"cell"] isEqualToString:@"CellRespuesta"] && [item[@"estado"] isEqualToString:@"1"]) {
            enviar=YES;
        }
    }
    
    if (enviar) {
        
        [SVProgressHUD show];
        
        NSMutableArray *list=[[NSMutableArray alloc]init];
        
        for (NSDictionary *item in container) {
            if ([item[@"cell"] isEqualToString:@"CellPregunta"]) {
                
                [list addObject:@{
                                  @"nombre":item[@"texto"],
                                  @"tipo":@"pregunta"
                                  }];
            }else if ([item[@"cell"] isEqualToString:@"CellRespuesta"]) {
                
                [list addObject:@{
                                  @"nombre":item[@"texto"],
                                  @"tipo":@"respuesta",
                                  @"res":item[@"estado"]
                                  }];
            }
        }
        
        FIRFirestore *db = [FIRFirestore firestore];
        [[db collectionWithPath:@"encuesta"] addDocumentWithData:@{@"preguntas":list} completion:^(NSError * _Nullable error) {
            
            [SVProgressHUD dismiss];
            if (error != nil) {
                FCAlertView *alert = [[FCAlertView alloc] init];
                
                [alert makeAlertTypeWarning];
                
                [alert showAlertInView:self
                             withTitle:nil
                          withSubtitle:@"Error enviando la información"
                       withCustomImage:nil
                   withDoneButtonTitle:@"OK"
                            andButtons:nil];
            } else {
                
                FCAlertView *alert = [[FCAlertView alloc] init];
                
                [alert makeAlertTypeSuccess];
                
                [alert showAlertInView:self
                             withTitle:nil
                          withSubtitle:@"Información enviada exitosamente."
                       withCustomImage:nil
                   withDoneButtonTitle:@"OK"
                            andButtons:nil];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
        
    }else{
        
        FCAlertView *alert = [[FCAlertView alloc] init];
        
        [alert makeAlertTypeWarning];
        
        [alert showAlertInView:self
                     withTitle:nil
                  withSubtitle:@"Debes seleccionar al menos una opción."
               withCustomImage:nil
           withDoneButtonTitle:@"OK"
                    andButtons:nil];
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)atras:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
