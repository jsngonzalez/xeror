//
//  CellGenerica.h
//  claroTeAyuda
//
//  Created by Jeisson González on 16/03/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellGenerica : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btn1;

@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UIImageView *img1;
@end
