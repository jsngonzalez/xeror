//
//  CellGenerica.m
//  claroTeAyuda
//
//  Created by Jeisson González on 16/03/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "CellGenerica.h"

@implementation CellGenerica

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
