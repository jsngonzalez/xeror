//
//  EquiposViewController.h
//  xerox
//
//  Created by Jeisson González on 11/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EquiposViewController : UIViewController<UITabBarDelegate,UITableViewDataSource>


@property (strong, nonatomic) IBOutlet UITableView *table;

@property (strong, nonatomic) IBOutlet UIImageView *bg;
@end
