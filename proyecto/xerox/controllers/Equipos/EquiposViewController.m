//
//  EquiposViewController.m
//  xerox
//
//  Created by Jeisson González on 11/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "EquiposViewController.h"
#import "DetalleEquipoViewController.h"
#import "CellGenerica.h"

@import Firebase;


@interface EquiposViewController ()

@end

@implementation EquiposViewController{
    NSMutableArray *container;
    NSMutableArray *_source;
    NSDictionary *seleccionado;
    
    int viewCargada;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initView];
    
    
    [FIRAnalytics logEventWithName:@"Equipos" parameters:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (viewCargada==0) {
        [self fillRecipe:0 source:_source];
        viewCargada=1;
    }
}

-(void)initView{
    
    
    CGRect frame = _bg.frame;
    frame.size.width = SCREEN_WIDTH + 30;
    _bg.frame = frame;
    
    [UIView animateWithDuration:0.8f
                     animations:^{
                         CGRect frame = _bg.frame;
                         frame.size.width = SCREEN_WIDTH+2;
                         _bg.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    _table.separatorColor = [UIColor clearColor];
    _table.separatorStyle = UITableViewCellSeparatorStyleNone;
    _table.backgroundColor=[UIColor clearColor];
    
    
    container=[[NSMutableArray alloc]init];
    _source=[[NSMutableArray alloc]initWithArray:@[
                                                     @{
                                                         @"cell":@"CellProducto",
                                                         @"nombre":@"Prensa digital Xerox® Iridesse™",
                                                         @"imagen":[UIImage imageNamed:@"Iridesse"],
                                                         @"mas_informacion":@"https://www.xerox.com/digital-printing/printers/digital-press/iridesse-production-press/esmx.html",
                                                         @"video":@"",
                                                         @"detalle":@"Hasta seis estaciones de tinta seca en línea para tintas secas opcionales Blanco, Transparente, Plateado y Dorado. Ofrece un sorprendente impacto en cada página impresa gracias a las mejoras digitales de especialidad y tecnología de color FLX.",
                                                         },
                                                     @{
                                                         @"cell":@"CellProducto2",
                                                         @"nombre":@"Prensa de inyección de tinta de producción en HD Xerox® Brenva®",
                                                         @"imagen":[UIImage imageNamed:@"Brenva"],
                                                         @"mas_informacion":@"https://www.xerox.com/digital-printing/digital-printing-press/color-printing/brenva-hd/esmx.html",
                                                         @"video":@"",
                                                         @"detalle":@"La pareja que tanto estaba esperando: la versatilidad de una plataforma de hoja de corte y la economía de una prensa de inyección de tinta."
                                                         },
                                                     @{
                                                         @"cell":@"CellProducto",
                                                         @"nombre":@"Prensa Xerox® Versant® 3100",
                                                         @"imagen":[UIImage imageNamed:@"Versant_3100"],
                                                         @"mas_informacion":@"https://www.xerox.com/digital-printing/digital-printing-press/color-printing/xerox-versant-3100/esmx.html",
                                                         @"video":@"",
                                                         @"detalle":@"Máxima automatización y más productividad para ayudarlo a satisfacer las más altas exigencias de producción\n\nColor totalmente automatizado y optimización de impresión de producción vía el sistema de Matriz de ancho completo\n\nResolución Ultra HD de 10 bits con cuatro veces más píxeles que el estándar",
                                                         },
                                                     @{
                                                         @"cell":@"CellProducto2",
                                                         @"nombre":@"Prensa Xerox® Versant® 180",
                                                         @"imagen":[UIImage imageNamed:@"Versant_180"],
                                                         @"mas_informacion":@"https://www.xerox.com/digital-printing/digital-printing-press/color-printing/xerox-versant-180/esmx.html",
                                                         @"video":@"",
                                                         @"detalle":@"Lleve su negocio de impresión al siguiente nivel con automatización y calidad en un tamaño compacto.\n\nLa opción con Paquete Performance ofrece 80 ppm en todos los papeles hasta 350 g/m², agrega la calibración de color automática con el espectrofotómetro incorporado en\n\nlínea X-Rite®\n\nResolución Ultra HD con cuatro veces más píxeles que el estándar"
                                                         },
                                                     @{
                                                         @"cell":@"CellProducto",
                                                         @"nombre":@"Copiadora/Impresora Xerox® D136",
                                                         @"imagen":[UIImage imageNamed:@"D136"],
                                                         @"mas_informacion":@"https://www.xerox.com/digital-printing/printers/copiers/xerox-d136/esmx.html",
                                                         @"video":@"",
                                                         @"detalle":@"Velocidad máxima: 136 PPM\n\nResolución: 2400 x 2400 ppp\n\nTamaño del material: Min: 102 x 152 mm\n\nMax: 330 x 488 mm\n\nVolumen promedio mensual recomendado: 70.000 - 700.000 páginas por mes",
                                                         },
                                                     @{
                                                         @"cell":@"CellTitulo"
                                                         },
                                                     @{
                                                         @"cell":@"CellProducto2",
                                                         @"nombre":@"XMPie®",
                                                         @"imagen":[UIImage imageNamed:@"XMPie"],
                                                         @"mas_informacion":@"https://www.xerox.com/digital-printing/workflow/printing-software/xmpie/esmx.html",
                                                         @"video":@"",
                                                         @"detalle":@"XMPie, el proveedor líder de software para marketing personalizado, crossmedia y con datos variables, ofrece soluciones que ayudan a las empresas a crear, gestionar, automatizar y monitorizar campañas de marketing y crossmedia directas y muy efectivas, sin sacrificar las funcionalidades de los medios, la presentación ni el diseño. ",
                                                         },
                                                     @{
                                                         @"cell":@"CellProducto",
                                                         @"nombre":@"Xerox Specialty Imaging Text",
                                                         @"imagen":[UIImage imageNamed:@"Specialty_Imaging"],
                                                         @"mas_informacion":@"https://www.xerox.com/es-mx/digital-printing/secure-printing",
                                                         @"video":@"",
                                                         @"detalle":@"La protección de los documentos confidenciales frente a duplicaciones no autorizadas es objeto de una creciente preocupación. Las copias falsificadas de documentos como cupones, billetes o facturas pueden tener un serio impacto financiero. Ahora puede combatir este problema con exclusivos efectos de imagen especiales destinados a evitar el fraude en trabajos estáticos y variables, añadiendo un nivel adicional de seguridad y autenticación. Estos efectos de texto especiales solamente se pueden imprimir con un procesador frontal digital FreeFlow y software FreeFlow; cualquier otro servidor de impresión los imprimirá como texto convencional.",
                                                         },
                                                     @{
                                                         @"cell":@"CellProducto2",
                                                         @"nombre":@"Color-Logic",
                                                         @"imagen":[UIImage imageNamed:@"color_logic"],
                                                         @"mas_informacion":@"http://www.color-logic.com/xerox/",
                                                         @"video":@"",
                                                         @"detalle":@"Nuestro catálogo de 'Mejores prácticas' le proporciona información útil sobre cómo generar archivos de Color-Logic para impresoras analógicas y digitales, y para impresiones de tinta blanca y plata.",
                                                         },
                                                     ]];
    viewCargada=0;
    
}


-(void)fillRecipe:(int)index source:(NSMutableArray *)source{
    
    
    [UIView animateWithDuration:.4 delay:0.01 options:UIViewAnimationOptionTransitionNone animations:^{
        
        [_table beginUpdates];
        
        [container addObject:source[index]];
        
        [_table insertRowsAtIndexPaths:@[
                                         [NSIndexPath indexPathForRow:container.count-1 inSection:0]
                                         ]
                      withRowAnimation:UITableViewRowAnimationTop];
        
        [_table endUpdates];
        
    } completion:^(BOOL finished) {
        
        int i=index+1;
        if (i<source.count) {
            [self fillRecipe:i source:source];
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableViewDalegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [container count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item=[container objectAtIndex:indexPath.row];
    
    
    CellGenerica *cell =[tableView dequeueReusableCellWithIdentifier:item[@"cell"]];
    cell.backgroundColor=[UIColor clearColor];
    
    if ([item[@"cell"] isEqualToString:@"CellProducto"] || [item[@"cell"] isEqualToString:@"CellProducto2"]) {
        
        cell.img1.layer.cornerRadius = cell.img1.frame.size.width / 2;
        cell.img1.clipsToBounds = YES;
        
        cell.img1.image=item[@"imagen"];
        cell.label1.text=item[@"nombre"];
    }
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *item=[container objectAtIndex:indexPath.row];
    
    if ([item[@"cell"] isEqualToString:@"CellProducto"] || [item[@"cell"] isEqualToString:@"CellProducto2"]) {
        return 160;
    }else{
        return 55;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    seleccionado=[container objectAtIndex:indexPath.row];
    NSLog(@"item:%@",seleccionado);
    
    if ([seleccionado[@"cell"] isEqualToString:@"CellProducto"] || [seleccionado[@"cell"] isEqualToString:@"CellProducto2"]) {
        [self performSegueWithIdentifier:@"seg_detalle" sender:self];
    }
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"seg_detalle"]) {
        DetalleEquipoViewController *temp=segue.destinationViewController;
        [temp setItem:seleccionado];
    }
    
}


- (IBAction)atras:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
