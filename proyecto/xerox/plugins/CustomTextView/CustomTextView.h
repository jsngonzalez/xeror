//
//  CustomTextView.h
//  claroTeAyuda
//
//  Created by Jeisson González on 22/03/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextView : UITextView<UITextViewDelegate>
@property (strong,nonatomic)NSString *placeholder;

@end
