//
//  CustomTextView.m
//  claroTeAyuda
//
//  Created by Jeisson González on 22/03/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import "CustomTextView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomTextView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    self.delegate=self;
    
    self.layer.masksToBounds=YES;
    [self.layer setCornerRadius:5.0f];
    
    self.layer.borderColor=[[UIColor colorWithRed:131.0f/255.0f green:134.0f/255.0f blue:136.0f/255.0f alpha:1.0] CGColor];
    self.layer.borderWidth=1.0;
    
    self.layer.opacity=0.0f;
    float flag=self.frame.origin.y;
    self.frame=CGRectMake(self.frame.origin.x, self.frame.origin.y+20, self.frame.size.width, self.frame.size.height);
    
    [UIView animateWithDuration:0.5 animations:^(void){
        self.layer.opacity=1.0f;
        self.frame=CGRectMake(self.frame.origin.x, flag, self.frame.size.width, self.frame.size.height);
    }];
    
    NSString *text=((_placeholder)?_placeholder:@"");
    
    if ([self.text isEqualToString:@""] || [self.text isEqualToString:text]) {
        self.text = text;
        self.textColor = [UIColor lightGrayColor];
    }else{
        self.textColor=[UIColor blackColor];
    }
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:((_placeholder)?_placeholder:@"")]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text =((_placeholder)?_placeholder:@"");
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

@end
