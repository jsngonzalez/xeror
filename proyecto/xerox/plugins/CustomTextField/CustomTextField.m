//
//  CustomTextField.m
//  phonendousr
//
//  Created by Jeisson González on 5/09/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "CustomTextField.h"
#import <QuartzCore/QuartzCore.h>

#define HEIGTH_CUSTOM_TEXTFIELD 40

@implementation CustomTextField

- (void)drawRect:(CGRect)rect {
    self.delegate=self;
    
    self.layer.masksToBounds=YES;
    [self.layer setCornerRadius:5.0f]; 
    
    
    UIColor *color=[UIColor colorWithRed:228.0f/255.0f green:228.0f/255.0f blue:228.0f/255.0f alpha:1.0];
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:((self.placeholder)?self.placeholder:@"") attributes:@{NSForegroundColorAttributeName: color}];
    
    self.layer.opacity=0.0f;
    float flag=self.frame.origin.y;
    self.frame=CGRectMake(self.frame.origin.x, self.frame.origin.y+20, self.frame.size.width, HEIGTH_CUSTOM_TEXTFIELD);
    
    [UIView animateWithDuration:0.5 animations:^(void){
        self.layer.opacity=1.0f;
        self.frame=CGRectMake(self.frame.origin.x, flag, self.frame.size.width, self.frame.size.height);
    }];
    
    
    //UIRectFill(rect);
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    @try {
        int limit=[[self valueForKey:@"maxlength"] intValue];
        
        if (self.text.length>limit) {
            self.text=[self.text substringToIndex:limit];
        }
    } @catch (NSException *exception) {
        return YES;
    }
    
    return YES;
    
}

-(void)textViewDidChange:(UITextView *)textView{
    int limit=[[self valueForKey:@"maxlength"] intValue];
    NSLog(@"Escribirendo");
    
    if (textView.text.length>limit) {
        textView.text=[textView.text substringToIndex:limit];
    }
}



@end
