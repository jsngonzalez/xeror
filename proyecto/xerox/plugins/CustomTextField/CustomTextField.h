//
//  CustomTextField.h
//  phonendousr
//
//  Created by Jeisson González on 5/09/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField<UITextFieldDelegate>
@property (strong,nonatomic)NSNumber *maxlength;
@property (strong,nonatomic)NSNumber *tipo;
@property (strong,nonatomic)NSString *nombre;

@end
