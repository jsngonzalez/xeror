//
//  Usuario.m
//  walmart
//
//  Created by Jeisson González on 3/09/15.
//  Copyright (c) 2015 hidesoft. All rights reserved.
//

#import "Services.h"

@implementation Services

+(void)get:(NSString *)url  completion:(void (^)(BOOL finished,NSDictionary *res))completion{
    
    
    //url=[NSString stringWithFormat:@"http://proxy.hidesoft.co/?s=%@",[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //[manager.requestSerializer setValue:x_api_key forHTTPHeaderField:@"x-api-key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"data: %@", responseObject);
        
        if ([responseObject[@"error"] intValue]==0) {
            
            if(completion)
                completion(YES,responseObject);
            
        }else{
            
            if(completion)
                completion(NO,responseObject);
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(completion)
            completion(NO,@{
                            @"error":@0,
                            @"response":msg_error_red
                            });
        
    }];
    
}

+(void)post:(NSString *)url parameters:(NSDictionary *)parameters  completion:(void (^)(BOOL finished,NSDictionary *res))completion{
    
    
    //url=[NSString stringWithFormat:@"http://proxy.hidesoft.co/?s=%@",[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
    
    AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
    [policy setAllowInvalidCertificates:YES];
    [policy setValidatesDomainName:NO];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setSecurityPolicy:policy];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //[manager.requestSerializer setValue:x_api_key forHTTPHeaderField:@"x-api-key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"data: %@", responseObject);
        if(completion)
            completion(NO,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(completion)
            completion(NO,@{
                            @"error":@0,
                            @"response":msg_error_red
                            });
        
    }];
}

+(void)delete:(NSString *)url parameters:(NSDictionary *)parameters  completion:(void (^)(BOOL finished,NSDictionary *res))completion{
    
    //url=[NSString stringWithFormat:@"http://proxy.hidesoft.co/?s=%@",[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
    
    AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
    [policy setAllowInvalidCertificates:YES];
    [policy setValidatesDomainName:NO];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setSecurityPolicy:policy];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //[manager.requestSerializer setValue:x_api_key forHTTPHeaderField:@"x-api-key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [manager DELETE:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"data: %@", responseObject);
        
        if ([responseObject[@"error"] intValue]==0) {
            
            if(completion)
                completion(YES,responseObject[@"response"]);
            
        }else{
            
            if(completion)
                completion(NO,responseObject);
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(completion)
            completion(NO,@{
                            @"error":@0,
                            @"response":msg_error_red
                            });
        
    }];
}

+(void)put:(NSString *)url parameters:(NSDictionary *)parameters  completion:(void (^)(BOOL finished,NSDictionary *res))completion{
    
    
    //url=[NSString stringWithFormat:@"http://proxy.hidesoft.co/?s=%@",[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
    
    AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
    [policy setAllowInvalidCertificates:YES];
    [policy setValidatesDomainName:NO];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setSecurityPolicy:policy];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //[manager.requestSerializer setValue:x_api_key forHTTPHeaderField:@"x-api-key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSDictionary *data=@{
                         @"data":parameters
                         };
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [manager PUT:url parameters:data success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"data: %@", responseObject);
        
        if ([responseObject[@"error"] intValue]==0) {
            
            if(completion)
                completion(YES,responseObject[@"response"]);
            
        }else{
            
            if(completion)
                completion(NO,responseObject);
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(completion)
            completion(NO,@{
                            @"error":@0,
                            @"response":msg_error_red
                            });
        
    }];
}

@end
