//
//  Usuario.h
//  walmart
//
//  Created by Jeisson González on 3/09/15.
//  Copyright (c) 2015 hidesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLSessionManager.h"
#import "AFHTTPRequestOperationManager.h"


@interface Services : NSObject

+(void)get:(NSString *)url  completion:(void (^)(BOOL finished,NSDictionary *res))completion;

+(void)put:(NSString *)url parameters:(NSDictionary *)parameters  completion:(void (^)(BOOL finished,NSDictionary *res))completion;

+(void)post:(NSString *)url parameters:(NSDictionary *)parameters  completion:(void (^)(BOOL finished,NSDictionary *res))completion;

+(void)delete:(NSString *)url parameters:(NSDictionary *)parameters  completion:(void (^)(BOOL finished,NSDictionary *res))completion;


@end
