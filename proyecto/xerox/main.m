//
//  main.m
//  xerox
//
//  Created by Jeisson González on 10/05/17.
//  Copyright © 2017 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
